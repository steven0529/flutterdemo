import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('MyFlutter'),
      ),
      body: MySnackBar(),
    ));
  }
}

class MyContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("我是TEXT"),
      color: Colors.yellow,
      alignment: Alignment.centerLeft,
    );
  }
}

class MyColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [Text("Test1"), Text("Test2"), Text("Test3"), Text("Test4")],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
      alignment: Alignment.centerLeft,
    );
  }
}

class MyListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> items = [];
    for (var i = 1; i <= 100; i++) items.add("Item$i");
    return Container(
      child: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ListTile(title: Text('${items[index]}'));
        },
      ),
      alignment: Alignment.centerLeft,
    );
  }
}

// ignore: must_be_immutable
class MySnackBar extends StatelessWidget {
  SnackBar snackBar = SnackBar(content: Text("我就是SnackBar"));
  @override
  Widget build(BuildContext context) {
    return Container(
        child: OutlineButton(
            child: Text('點我'),
            onPressed: () {
              Scaffold.of(context).showSnackBar(snackBar);
            }),
        alignment: Alignment.center);
  }
}
